# 進捗報告書・梗概用クラスファイル progress_report.cls

## 含まれるファイル

<dl>
  <dt>.gitignore</dt>
    <dd>余分なファイルをgitリポジトリに登録しないための設定ファイル</dd>
  <dt>.gitmodules</dt>
    <dd>submodule（jecon-bst）を登録しているファイル</dd>
  <dt>.latexmkrc</dt>
    <dd>LaTeXのコンパイルに用いるlatexmkの設定ファイル</dd>
  <dt>README.md</dt>
    <dd>導入方法等の説明（このファイル）</dd>
  <dt>jecon-bst/</dt>
    <dd>bibtex用のスタイルファイル一式（git submoduleとして追加されている）</dd>
  <dt>library_sample.bib</dt>
    <dd>説明書で使用しているbibファイル（manual.texのコンパイルに必要）</dd>
  <dt>manual.tex</dt>
    <dd>クラスファイルの使い方とLaTeXの簡易説明書</dd>
  <dt>progress.tex</dt>
    <dd>進捗報告書の書き方の説明書</dd>
  <dt>progress_report.cls</dt>
    <dd>進捗報告書用クラスファイル</dd>
  <dt>sample/</dt>
    <dd>説明書で使用しているプログラムのファイルが入っているディレクトリ</dd>
  <dt>summary.tex</dt>
    <dd>梗概の書き方の説明書</dd>
</dl>

## プロジェクトのfork

以下の説明は，gitとLaTeX（TeX Live 2018以降，またはMac Tex 2018以降）の環境が用意されている環境を想定している．
年度初めに環境構築を正しく行っていれば，この要件を満たす環境が構築されている．

まず，GitLab上でこのリポジトリ（progress_report）を自身のリポジトリとしてforkする．
forkの手順は以下の通り．
「1.」の手順を行うとページが移動するため，事前に「2.」の手順までよく読んでから操作を始めること．

1. このWebページを一番上までスクロールすると，右上に「Fork」（または「フォーク」）というボタンがあるので，そこをクリックする
2. 「Select a namespace to fork the project」（または「プロジェクトをフォークする名前空間を選択してください」）という指示が表示されるので，自身のユーザを選択する

以上の操作により，https://gitlab.com/（GitLabでのユーザ名）/progress_report というリポジトリが作成される．

ターミナルソフト（iTerm.app，PowerShellなど）で，下記のコマンドを実行し，forkしたリポジトリをcloneする．
以下の例では，自身のDropboxのフォルダ直下にcloneしているが，好みに合わせて別のディレクトリにcloneしてもよい．
ただし，研究に関わるデータであるため，少なくともDropboxのフォルダ以下に置いておくこと．

なお，%マークから始まる行が入力すべきコマンドを示し，他の行はプログラムの出力を示す．

最後のgit cloneを行うコマンドを実行する際に指定するURL（git@以下の部分）については，Webページ右上の「Clone」ボタン→「Clone with SSH」からコピーできる．

```zsh
% cd ~
% cd 'Dropbox (ELT Lab.)'
% git clone --recursive git@gitlab.com:（GitLabでのユーザ名）/progress_report.git
Enter passphrase for key './id_rsa': （ssh秘密鍵のパスフレーズを入力）
（中略）
Resolving deltas: 100% (68/68), done.
（中略）
Submodule path 'jecon-bst': checked out '1b65ca7eb543254bc09aa74b19cc3f8d2cdb6591'
% cd progress_report
```

上記の```Resolving deltas: 100% (68/68), done.```のような（68/68の部分はバージョンによって異なる）表示が出れば，正しくcloneできている．

### クラスファイルのバージョンアップに備える

今後，このクラスファイルがバージョンアップされることに備え，cloneしたあとに以下のコマンドを実行しておくこと．

これ以降のコマンドは，カレントディレクトリがprogress_reportディレクトリになっていることを想定している．

```zsh
% git remote add upstream git@gitlab.com:eltlab/progress_report.git
```

クラスファイルがバージョンアップされた場合には，以下のコマンドを実行する．

```zsh
% git fetch upstream
From https://gitlab.com/（GitLabでのユーザ名）/progress_report
 * [new branch]      master     -> upstream/master
% git merge upstream/master
Already up to date. （手元に最新のバージョンがある場合は，このように表示される）
```

## 説明書のコンパイル

上でリポジトリをcloneした，progress_reportディレクトリに移動（cd）してから，以下のコマンドを実行すると，説明書がコンパイルされる．

```zsh
% latexmk
Latexmk: This is Latexmk, John Collins, 26 Dec. 2019, version: 4.67.
Rule 'lualatex': The following rules & subrules became out-of-date:
      'lualatex'
------------
Run number 1 of rule 'lualatex'
------------
（中略）
Latexmk: All targets () are up-to-date
```

プログラムの実行が終わると，同じディレクトリ内に3つの説明書（manual.pdf, progress.pdf, summary.pdf）ができているので，まずはこれを読むこと．

また，説明書のソースファイル（manual.tex）自体がクラスファイル（progress_report.cls）の使用例となっているため，manual.pdfと比べながら内容を確認すること．

自身の進捗報告書を最初に作成する際には，progress.texをコピーすること．
コピー先のファイル名は，「progress_自身の名字.tex」（例：progress_kunimune.tex）のようにすること．

なお，梗概については進捗報告書と区別するために，summary.texを「summary_自身の名字.tex」のようなファイル名にコピーして作成する．

Visual Studio Codeでtexファイルを編集し保存すると，自動的にコンパイルが行われる．
手動でコンパイルする場合は，texファイルと同じディレクトリで```latexmk progress_kunimune.tex```のようにlatexmkコマンドを実行する．

## 進捗報告書の管理

gitを用いて適切にバージョン管理（commit）すること．
また，定期的にGitLabにpushすること．
pushすることによって，バックアップ先が1カ所増えることになり，万が一の事態になったときの救いになる．

commitを行う際には，なるべく変更内容が後で分かるようなメッセージを付ける．
逆にいえば，変更内容を一言で表せるような作業の単位でcommitを行うとよい．

例えば，「1章を〜が分かるように書き換えた」とか，「〜を示す表とそれについて説明する本文を追加した」のような単位でcommitできることが望ましい．

ただし，変更内容を一言で表せないからといって，commitすることをためらってはいけない．
とにかくcommitしておけば，手戻り（一度消してしまった文章や図表の復活など）が容易になる．

また，進捗報告会ごとにcommitすることが望ましい．
その際のcommitメッセージについて，例えば「○月×日分の進捗報告書」のようにするとよい．

texファイルから生成されるPDFファイルについては，gitで管理されないため，進捗報告会のたびに「progress_自分の名字_20210401.pdf」のように日付を付けたファイル名でコピーしておくこと．